module Tflow
  class ApiClient
    module OrderItem
      # http://enterprise.tucannadev.com/apidoc/#api-OrderItem-CreateOrderItem

      def create_order_item
        conn = @client.post do |req|
          req.url '/api/v2/orderItem/create'
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-OrderItem-DeleteOrderItem

      def delete_order_item
        conn = @client.delete do |req|
          req.url "/api/v2/orderItem/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-OrderItem-GetOrderItem
      def get_order_item
        conn = @client.get do |req|
          req.url "/api/v2/orderItem/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-OrderItem-GetOrderItemList

      def list_order_items
        conn = @client.get do |req|
          req.url '/api/v2/orderItem/list?'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-OrderItem-UpdateOrderItem

      def update_order_item(id, json_payload)
        conn = @client.post do |req|
          req.url "/api/v2/orderItem/#{id}/update"
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end
    end
  end
end
