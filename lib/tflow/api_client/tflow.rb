module Tflow
  class ApiClient
    module Tflow
      # http://enterprise.tucannadev.com/apidoc/#api-Tflow-DeleteTflowQueue

      def delete_tflow(id)
        conn = @client.delete do |req|
          req.url "/api/v2/tflow/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Tflow-GetTflow

      def get_tflow(id)
        conn = @client.get do |req|
          req.url "/api/v2/tflow/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Tflow-GetTflowList

      def list_tflows(json_payload = {})
        conn = @client.get do |req|
          req.url '/api/v2/tflow/list?'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Tflow-GetTflowsForEntity

      def get_entity_tflows(entity_type, entity_id)
        conn = @client.get do |req|
          req.url "/api/v2/tflow/#{entity_type}/#{entity_id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Tflow-CreateTflows

      def register_non_autodeleting_tflow(json_payload)
        conn = @client.post do |req|
          req.url '/api/v2/tflow/create'
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Tflow-SetTflowsForEntity

      def set_tflow_queue(entity_type, entity_id, json_payload)
        conn = @client.post do |req|
          req.url "/api/v2/tflow/#{entity_type}/#{entity_id}"
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end
    end
  end
end
