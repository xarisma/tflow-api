module Tflow
  class ApiClient
    module Profile
      # http://enterprise.tucannadev.com/apidoc/#api-Profile-GetProfileList

      def list_profiles
        conn = @client.get do |req|
          req.url '/api/v2/profile/list?'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end
    end
  end
end
