module Tflow
  class ApiClient
    module Permission
      # http://enterprise.tucannadev.com/apidoc/#api-Permission-GetPermissionList

      def list_permissions
        conn = @client.get do |req|
          req.url '/api/v2/permission/list'
          req.headers['Authorization'] = @token
        end
        conn.body
      end
    end
  end
end
