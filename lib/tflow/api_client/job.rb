module Tflow
  class ApiClient
    module Job
      # http://enterprise.tucannadev.com/apidoc/#api-Job-ClearRush

      def clear_rush_request_job(id)
        conn = @client.post do |req|
          req.url "/api/v2/job/#{id}/rush/clear"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-CreateJob

      def create_job(json_payload)
        conn = @client.post do |req|
          req.url '/api/v2/job/create'
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-DeleteJob

      def delete_job(id)
        conn = @client.delete do |req|
          req.url "/api/v2/job/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-ExecuteTransition

      def executeTransition(id, json_payload)
        conn = @client.post do |req|
          req.url "/api/v2/job/#{id}/executeTransition"
          req.headers['Authorization'] = @token
          req.headers['Content-Type'] = 'multipart/form-data'
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-GetJobInitialInfo

      def get_initial_job_info(json_payload)
        conn = @client.get do |req|
          req.url '/api/v2/job/getInitialInfo'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-GetJob

      def get_job(id)
        conn = @client.get do |req|
          req.url "/api/v2/job/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-GetJobList

      def list_jobs(json_payload = {})
        conn = @client.get do |req|
          req.url '/api/v2/job/list?'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-GetAllowedTransitions

      def list_job_transitions(id)
        conn = @client.get do |req|
          req.url "/api/v2/job/#{id}/allowedTransitions"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-GetRevisions

      def list_job_revisions(id)
        conn = @client.get do |req|
          req.url "/api/v2/job/#{id}/revisions"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-RepreflightJob

      def re_preflight(id)
        conn = @client.post do |req|
          req.url "/api/v2/job/#{id}/repreflight"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-SendRushRequest

      def set_rush_job(id)
        conn = @client.post do |req|
          req.url "/api/v2/job/#{id}/rush/request"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-UndoRequestRush

      def undo_rush_request(id)
        conn = @client.post do |req|
          req.url "/api/v2/job/#{id}/rush/undoRequest"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-UpdateJob

      def update_job(id, json_payload)
        conn = @client.post do |req|
          req.url "/api/v2/job/#{id}/update"
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Job-UploadAndCreateJob

      def upload_artwork_and_create(json_payload)
        conn = @client.post do |req|
          req.url '/api/v2/job/uploadAndCreate'
          req.headers['Authorization'] = @token
          req.headers['Content-Type'] = 'multipart/form-data'
          req.body = json_payload
        end
        conn.body
      end
    end
  end
end
