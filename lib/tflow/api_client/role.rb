module Tflow
  class ApiClient
    module Role
      # http://enterprise.tucannadev.com/apidoc/#api-Role-CreateRole

      def create_role(json_payload)
        conn = @client.post do |req|
          req.url '/api/v2/role/create'
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Role-GetRole

      def get_role(id)
        conn = @client.get do |req|
          req.url "/api/v2/role/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Role-GetRoleUsers

      def get_assigned_users(id, json_payload = {})
        conn = @client.get do |req|
          req.url "/api/v2/role/#{id}/getUsers"
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Role-GetRolePermissions

      def list_roles(json_payload = {})
        conn = @client.get do |req|
          req.url '/api/v2/role/list'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Role-GetRolePermissions

      def get_permissions
        conn = @client.get do |req|
          req.url '/api/v2/role/getPermissions'
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Role-GetRoleWorkflowPermissions

      def get_workflow_permissions
        conn = @client.get do |req|
          req.url '/api/v2/role/getWorkflowPermissions'
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Role-SetRoleUsers

      def set_assigned_users(id, json_payload)
        conn = @client.post do |req|
          req.url "/api/v2/role/#{id}/setUsers"
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Role-SetRolePermissions

      def set_role_permissions(json_payload)
        conn = @client.post do |req|
          req.url '/api/v2/role/setPermissions'
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Role-SetRoleWorkflowPermissions

      def set_workflow_permisions(json_payload)
        conn = @client.post do |req|
          req.url '/api/v2/role/setWorkflowPermissions'
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Role-UpdateRole

      def update_role(id, json_payload)
        conn = @client.post do |req|
          req.url "/api/v2/role/#{id}"
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end
    end
  end
end
