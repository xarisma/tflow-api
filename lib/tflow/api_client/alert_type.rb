module Tflow
  class ApiClient
    module AlertType
      # http://enterprise.tucannadev.com/apidoc/#api-AlertType-GetAlertTypeList

      def list_alert_types(json_payload = {})
        conn = @client.get do |req|
          req.url '/api/v2/alertType/list?'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end
    end
  end
end
