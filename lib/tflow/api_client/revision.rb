module Tflow
  class ApiClient
    module Revision
      # http://enterprise.tucannadev.com/apidoc/#api-Revision-DeleteRevision

      def delete_revision(id)
        conn = @client.delete do |req|
          req.url "/api/v2/revision/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Revision-GetLowresFile

      def download_lowres_pdf(id)
        conn = @client.get do |req|
          req.url "/api/v2/revision/#{id}/lowresFile"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Revision-GetOriginalFile

      def download_original_file(id)
        conn = @client.get do |req|
          req.url "/api/v2/revision/#{id}/originalFile"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Revision-GetPreflightReportXmlFile

      def download_preflight_xml(id)
        conn = @client.get do |req|
          req.url "/api/v2/revision/#{id}/preflightReportXml"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Revision-GetPreflightedPdf

      def download_preflighted_pdf(id)
        conn = @client.get do |req|
          req.url "/api/v2/revision/#{id}/preflightedPdf"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Revision-GetPagePreview

      def download_preview_image(id, pageNo)
        conn = @client.get do |req|
          req.url "/api/v2/revision/#{id}/preview/#{pageNo}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Revision-GetProof

      def download_proof_pdf(id)
        conn = @client.get do |req|
          req.url "/api/v2/revision/#{id}/proof"
          req.headers['Authorization'] = @token
        end
        conn.body
      end
    end
  end
end
