module Tflow
  class ApiClient
    module Order
      # http://enterprise.tucannadev.com/apidoc/#api-Order-CreateOrder

      def create_order(json_payload)
        conn = @client.post do |req|
          req.url '/api/v2/order/create'
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Order-DeleteOrder

      def delete_order(id)
        conn = @client.delete do |req|
          req.url "/api/v2/job/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Order-GetOrderInitialInfo

      def get_order_inital_info(json_payload)
        conn = @client.get do |req|
          req.url '/api/v2/order/getInitialInfo'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Order-GetOrder

      def get_order(id)
        conn = @client.get do |req|
          req.url "/api/v2/order/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Order-GetOrderList

      def list_orders(json_payload = {})
        conn = @client.get do |req|
          req.url '/api/v2/order/list?'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Order-UpdateOrder

      def update_order(id, json_payload)
        conn = @client.post do |req|
          req.url "/api/v2/order/#{id}/update"
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end
    end
  end
end
