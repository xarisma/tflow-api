module Tflow
  class ApiClient
    module Client
      # http://enterprise.tucannadev.com/apidoc/#api-Client-CreateClient

      def create_client(json_payload)
        conn = @client.post do |req|
          req.url '/api/v2/client/create'
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Client-GetClientInitialInfo

      def get_client_initial_info
        conn = @client.get do |req|
          req.url '/api/v2/client/getInitialInfo'
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Client-GetClient

      def get_client(id)
        conn = @client.get do |req|
          req.url "/api/v2/client/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Client-GetClientList

      def list_clients(json_payload = {})
        conn = @client.get do |req|
          req.url '/api/v2/client/list?'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Client-GetClientTflows

      def get_client_tflows(id)
        conn = @client.get do |req|
          req.url "/api/v2/client/#{id}/tflows"
          req.headers['Authorization'] = @token
        end
        conn.body
      end
    end
  end
end
