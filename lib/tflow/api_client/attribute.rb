module Tflow
  class ApiClient
    module Attribute
      # http://enterprise.tucannadev.com/apidoc/#api-Attribute-CreateAttribute

      def create_attribute(entityt_type, entity_id, json_payload = {})
        conn = @client.get do |req|
          req.url "/api/v2/attribute/#{entityt_type}/#{entity_id}?"
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Attribute-DeleteAttribute

      def delete_attribute(id)
        conn = @client.delete do |req|
          req.url "/api/v2/attribute/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Attribute-GetAttributeList

      def list_attributes(entityt_type, entity_id, json_payload = {})
        conn = @client.get do |req|
          req.url "/api/v2/attribute/list/#{entityt_type}/#{entity_id}?"
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Attribute-GetAttributeList

      def update_attribute(id, json_payload = {})
        conn = @client.post do |req|
          req.url "/api/v2/attribute/#{id}"
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end
    end
  end
end
