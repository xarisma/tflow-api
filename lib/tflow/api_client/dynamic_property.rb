module Tflow
  class ApiClient
    module DynamicProperties
      # http://enterprise.tucannadev.com/apidoc/#api-DynamicProperties-GetDefinitions

      def get_dynamic_properties(entity_type)
        conn = @client.get do |req|
          req.url "/api/v2/prop/#{entity_type}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end
    end
  end
end
