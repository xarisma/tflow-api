module Tflow
  class ApiClient
    module User
      # http://enterprise.tucannadev.com/apidoc/#api-User-CreateUser

      def create_user(json_payload)
        conn = @client.post do |req|
          req.url '/api/v2/user/create'
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-User-GetUserInitialInfo

      def get_user_initial_info(json_payload)
        conn = @client.get do |req|
          req.url '/api/v2/user/getInitialInfo'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-User-GetUser

      def get_user(id)
        conn = @client.get do |req|
          req.url "/api/v2/user/#{id}"
          req.headers['Authorization'] = @token
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-User-GetUserList

      def list_users(json_payload = {})
        conn = @client.get do |req|
          req.url '/api/v2/user/list?'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-User-UpdateUser

      def update_user(id, json_payload)
        conn = @client.post do |req|
          req.url "/api/v2/user/#{id}"
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end
    end
  end
end
