module Tflow
  class ApiClient
    module TflowDownload
      # http://enterprise.tucannadev.com/apidoc/#api-TflowDownload-CheckReadyJobs

      def list_ready_jobs(json_payload = {})
        conn = @client.get do |req|
          req.url '/api/v2/tflowDownload/checkReady?'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-TflowDownload-SetJobDownloadingState

      def set_download_state(id, json_payload)
        conn = @client.post do |req|
          req.url "/api/v2/tflowDownload/#{id}/setState"
          req.headers['Authorization'] = @token
          req.body = json_payload
        end
        conn.body
      end
    end
  end
end
