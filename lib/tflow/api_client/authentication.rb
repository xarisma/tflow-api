module Tflow
  class ApiClient
    module Authentication
      # http://enterprise.tucannadev.com/apidoc/#api-Authentication-GetApiVersion
      def version
        conn = @client.get do |req|
          req.url '/api/version'
        end
        conn.body
      end

      # http://enterprise.tucannadev.com/apidoc/#api-Authentication-GetAccessToken

      def authentication(client_id, client_secret, grant_type = "client_credentials")
        conn = @client.post do |req|
          req.url '/oauth/access_token'
          req.body = {"grant_type": grant_type, "client_id": client_id, "client_secret": client_secret}
        end
        @token = "#{conn.body["token_type"]} #{conn.body["access_token"]}"
      end
    end
  end
end
