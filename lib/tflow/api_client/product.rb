module Tflow
  class ApiClient
    module Product
      # http://enterprise.tucannadev.com/apidoc/#api-Product-GetProductList

      def list_products(json_payload = {})
        conn = @client.get do |req|
          req.url '/api/v2/product/list'
          req.headers['Authorization'] = @token
          req.params = json_payload
        end
        conn.body
      end
    end
  end
end
